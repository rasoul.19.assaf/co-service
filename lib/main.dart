import 'package:co_services_students/data/repositories/authentication_repository.dart';
import 'package:co_services_students/data/repositories/firestore_repository.dart';
import 'package:co_services_students/logic/cubit/navigation_cubit.dart';
import 'package:co_services_students/presentation/Screens/authentication/landing_screen.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import 'core/main_constant.dart';
import 'firebase_options.dart';
import 'presentation/Screens/home_screen.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  try {
    dynamic result = await Firebase.initializeApp(
      options: DefaultFirebaseOptions.currentPlatform,
    );
    FirebaseAuth.instance.setPersistence(Persistence.LOCAL);
    print(result);
  } catch (e) {
    print(e);

    print('error firebase connection');
  }

  SystemChrome.setSystemUIOverlayStyle(
    SystemUiOverlayStyle(
      statusBarColor: kGreenCOLOR,
      statusBarBrightness: Brightness.light,
      statusBarIconBrightness: Brightness.light,
      systemNavigationBarColor: kGreenCOLOR.withOpacity(0.0),
    ),
  );
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ScreenUtilInit(
      designSize: const Size(360, 640),
      minTextAdapt: true,
      splitScreenMode: true,
      builder: (BuildContext context) {
        return MaterialApp(
          debugShowCheckedModeBanner: false,
          builder: (context, widget) {
            ScreenUtil.setContext(context);
            return MediaQuery(
              data: MediaQuery.of(context).copyWith(textScaleFactor: 1.0),
              child: widget!,
            );
          },
          home: Builder(
            builder: (context) {
              if (AuthenticationRepository.instance.checkSignedIn()) {
                return FutureBuilder(
                    future: FirestoreRepository.instance.initCurrentUser(FirebaseAuth.instance.currentUser!),
                    builder: (BuildContext context, AsyncSnapshot<void> snapshot) {
                      if (snapshot.hasData) {
                        return BlocProvider(
                          create: (context) => NavigationCubit(),
                          child: HomeScreen(),
                        );
                      } else {
                        return Center(
                          child: CircularProgressIndicator(),
                        );
                      }
                    });
              }
              return LandingScreen();
            },
          ),
        );
      },
    );
  }
}
