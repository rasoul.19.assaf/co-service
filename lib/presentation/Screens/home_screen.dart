import 'package:co_services_students/core/main_constant.dart';
import 'package:co_services_students/data/models/service.dart';
import 'package:co_services_students/data/repositories/firestore_repository.dart';
import 'package:co_services_students/logic/cubit/navigation_cubit.dart';
import 'package:co_services_students/logic/cubit/services_cubit.dart';
import 'package:co_services_students/presentation/Screens/add_service_screen.dart';
import 'package:co_services_students/presentation/Screens/categories_screen.dart';
import 'package:co_services_students/presentation/Screens/chats_list_screen.dart';
import 'package:co_services_students/presentation/Screens/profile_page.dart';
import 'package:co_services_students/presentation/widgets/service_card.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class HomeScreen extends StatefulWidget {
  List homeNav = [
    MultiBlocProvider(providers: [
      BlocProvider(
        create: (BuildContext context) => ServicesCubit(),
      ),
    ], child: MainHome()),
  ];

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  int currentIndex = 0;
  late Widget categoriesNav;
  late Widget addServiceNav;
  late Widget chatNav;

  @override
  void initState() {
    addServiceNav = const AddServiceScreen();
    categoriesNav = CategoriesScreen(onWillPop: () async {
      context.read<NavigationCubit>().setIndex(0);
      return true;
    });
    chatNav = ChatsListScreen();
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        print('pop');
        setState(() {
          currentIndex = 1;
        });
        return true;
      },
      child: SafeArea(
        child: Scaffold(
          backgroundColor: Colors.transparent,
          bottomNavigationBar: buildBottomNavigationBar(context),
          body: BlocBuilder<NavigationCubit, NavigationState>(
            builder: (context, state) {
              if (state.index == 0) {
                if (state.homeNav.isEmpty) {
                  context.read<NavigationCubit>().pushToHome(
                        MultiBlocProvider(providers: [
                          BlocProvider(
                            create: (BuildContext context) => ServicesCubit(),
                          ),
                        ], child: MainHome()),
                      );
                }
              }
              int index = state.index;
              print("building ");
              print(index);
              if (index == 0) {
                widget.homeNav = state.homeNav;
              }
              return buildBody(index);
            },
          ),
        ),
      ),
    );
  }

  Widget buildBottomNavigationBar(BuildContext context) {
    return BottomNavigationBar(
      currentIndex: context.watch<NavigationCubit>().state.index,
      selectedItemColor: kBlackColor,
      unselectedItemColor: Colors.grey,
      showSelectedLabels: false,
      showUnselectedLabels: false,
      iconSize: 24,
      onTap: (index) {
        context.read<NavigationCubit>().setIndex(index);
      },
      items: const [
        BottomNavigationBarItem(label: 'home', icon: Icon(Icons.home_filled)),
        BottomNavigationBarItem(label: 'home', icon: Icon(Icons.search)),
        BottomNavigationBarItem(label: 'home', icon: Icon(Icons.add)),
        BottomNavigationBarItem(label: 'home', icon: Icon(Icons.message)),
        BottomNavigationBarItem(label: 'home', icon: Icon(Icons.notifications_none_sharp)),
      ],
    );
  }

  Widget buildBody(int currentIndex) {
    // IF selected service
    // return homeNav .add(selectedServiceScreen)
    if (currentIndex == 0 || currentIndex == 4) {
      print(widget.homeNav.length);
      return widget.homeNav.last;
    } else if (currentIndex == 2) {
      return addServiceNav;
    } else if (currentIndex == 1) {
      return categoriesNav;
    } else if (currentIndex == 3) {
      return chatNav;
    }
    return Container();
  }
}

class MainHome extends StatefulWidget {
  @override
  _MainHomeState createState() => _MainHomeState();
}

class _MainHomeState extends State<MainHome> {
  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: const BoxDecoration(
        image: DecorationImage(
          image: AssetImage("assets/images/cover.jpg"),
          fit: BoxFit.cover,
        ),
      ),
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 12.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            const SizedBox(
              height: 20,
            ),
            Align(
              alignment: Alignment.centerRight,
              child: GestureDetector(
                onTap: () async {
                  if (FirestoreRepository.instance.currentUser == null) {
                    await FirestoreRepository.instance.initCurrentUser(FirebaseAuth.instance.currentUser!);
                  }
                  context.read<NavigationCubit>().pushToHome(ProfilePage());
                },
                child: Container(
                  width: 60,
                  height: 60,
                  padding: EdgeInsets.all(8.0),
                  decoration: const BoxDecoration(
                    color: Colors.white,
                    shape: BoxShape.rectangle,
                  ),
                  //TODO: add user avatar
                  child: FirebaseAuth.instance.currentUser!.photoURL != null
                      ? Container(
                          width: 50.sp,
                          height: 50.sp,
                          child: ClipRRect(
                            borderRadius: const BorderRadius.all(Radius.circular(50)),
                            child: Image.network(
                              FirebaseAuth.instance.currentUser!.photoURL!,
                              fit: BoxFit.cover,
                            ),
                          ),
                        )
                      : Icon(
                          Icons.account_circle_outlined,
                          size: 45.sp,
                        ),
                ),
              ),
            ),
            SizedBox(
                // height: 10,
                ),
            Container(
                margin: EdgeInsets.only(
                  right: 70,
                ),
                width: double.infinity,
                height: 40,
                decoration: BoxDecoration(
                  color: Color(0xFFE1E1E1),
                  borderRadius: BorderRadius.circular(20),
                ),
                child: const TextField(
                  textAlignVertical: TextAlignVertical.center,
                  decoration: InputDecoration(
                    suffixIcon: Icon(
                      Icons.search,
                      color: kBlackColor,
                    ),
                    isDense: true,
                    border: InputBorder.none,
                    hintText: 'search bar',
                    contentPadding: EdgeInsets.symmetric(
                      horizontal: 20,
                    ),
                  ),
                )),
            GestureDetector(
              onTap: () {
                context.read<ServicesCubit>().load();
              },
              child: Icon(Icons.refresh),
            ),
            SizedBox(
              height: 20,
            ),
            Expanded(
              child: BlocBuilder<ServicesCubit, ServicesState>(
                builder: (context, state) {
                  if (state is! ServicesLoaded) {
                    return Center(
                      child: CircularProgressIndicator(),
                    );
                  }
                  List<Service> services = state.services;
                  return ListView.builder(
                    itemCount: services.length,
                    itemBuilder: (context, index) {
                      return Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 8.0, vertical: 10),
                        child: ServiceCard(
                          service: services[index],
                          image: Image.network(
                            services[index].imageUrl ??
                                "https://www.pluralsight.com/content/pluralsight/en/blog/creative-professional/10-/10-essential-skills-need-motion-designer/_jcr_content/main/hero_blog_block/image-res.img.jpg/1527709354833.jpg",
                            fit: BoxFit.fitHeight,
                          ),
                          text: services[index].header,
                        ),
                      );
                    },
                  );
                },
              ),
            ),
          ],
        ),
      ),
    );
  }
}
