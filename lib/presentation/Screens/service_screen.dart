import 'package:co_services_students/core/main_constant.dart';
import 'package:co_services_students/data/models/service.dart';
import 'package:co_services_students/data/repositories/firestore_repository.dart';
import 'package:co_services_students/logic/cubit/navigation_cubit.dart';
import 'package:co_services_students/presentation/Screens/chatting_screen.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class ServiceScreen extends StatelessWidget {
  final Service service;

  const ServiceScreen({Key? key, required this.service}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        decoration: const BoxDecoration(
          image: DecorationImage(
            image: AssetImage("assets/images/tob.jpg"),
            fit: BoxFit.cover,
          ),
        ),
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              GestureDetector(
                onTap: () {
                  context.read<NavigationCubit>().popFromHome();
                },
                child: Padding(
                  padding: EdgeInsets.only(left: 20.w, right: 20.w),
                  child: SizedBox(
                    width: getScreenWidth(context),
                    height: 50.h,
                    child: Row(
                      children: [
                        const Icon(
                          Icons.arrow_back_ios_outlined,
                          color: Colors.white70,
                        ),
                        Text(
                          "Back",
                          style: TextStyle(color: Colors.white70, fontSize: 20.sp),
                        )
                      ],
                    ),
                  ),
                ),
              ),
              SizedBox(
                height: 30,
              ),
              Padding(
                padding: const EdgeInsets.all(3.0),
                child: Image.network(service.imageUrl ??
                    "https://www.pluralsight.com/content/pluralsight/en/blog/creative-professional/10-/10-essential-skills-need-motion-designer/_jcr_content/main/hero_blog_block/image-res.img.jpg/1527709354833.jpg"),
              ),
              SizedBox(
                height: 20,
              ),
              Container(
                padding: EdgeInsets.all(22.w),
                width: getScreenWidth(context) - 20.w,
                decoration: BoxDecoration(
                  color: Colors.grey[100],
                  boxShadow: [
                    BoxShadow(
                      color: Colors.grey.withOpacity(0.5),
                      spreadRadius: 1,
                      blurRadius: 2,
                      offset: const Offset(0, 0), // changes position of shadow
                    ),
                  ],
                  borderRadius: const BorderRadius.all(Radius.circular(40)),
                ),
                child: Text(
                  service.content ??
                      "Welcome to my service :)\n\n I do Machine Learning projects professionally using the programming",
                  style: TextStyle(fontSize: 16.sp, color: Colors.grey),
                ),
              ),
              SizedBox(
                height: 30,
              ),
              ElevatedButton(
                onPressed: () {
                  if (service.providerId == FirestoreRepository.instance.currentUser.id) {
                    return;
                  }
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => ChattingScreen(
                        otherId: service.providerId,
                      ),
                    ),
                  );
                },
                style: kButtonStyleSecondary,
                child: Text("Service Request"),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
