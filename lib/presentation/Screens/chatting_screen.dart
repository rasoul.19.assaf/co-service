import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:co_services_students/core/main_constant.dart';
import 'package:co_services_students/data/models/app_user.dart';
import 'package:co_services_students/data/models/message.dart';
import 'package:co_services_students/data/repositories/firestore_repository.dart';
import 'package:co_services_students/presentation/Screens/payment/choose_payment_method.dart';
import 'package:co_services_students/presentation/widgets/message_bubble.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class ChattingScreen extends StatefulWidget {
  final otherId;
  final name;

  const ChattingScreen({Key? key, this.otherId, this.name}) : super(key: key);

  @override
  _ChattingScreenState createState() => _ChattingScreenState();
}

class _ChattingScreenState extends State<ChattingScreen> {
  final TextEditingController _textFieldController = TextEditingController();

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: Colors.transparent,
        body: Container(
          height: getScreenHeight(context),
          decoration: const BoxDecoration(
            image: DecorationImage(
              image: AssetImage("assets/images/tob.jpg"),
              fit: BoxFit.cover,
            ),
          ),
          child: Padding(
            padding: EdgeInsets.only(left: 12.w, right: 12.w),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                GestureDetector(
                  onTap: () {
                    Navigator.of(context).pop();
                  },
                  child: SizedBox(
                    width: getScreenWidth(context),
                    height: 50.h,
                    child: Row(
                      children: [
                        const Icon(
                          Icons.arrow_back_ios_outlined,
                          color: Colors.white70,
                        ),
                        Text(
                          "Back",
                          style: TextStyle(color: Colors.white70, fontSize: 20.sp),
                        ),
                        Spacer(),
                        GestureDetector(
                          onTap: () {
                            Navigator.push(
                              context,
                              MaterialPageRoute(
                                builder: (context) => ChoosePaymentMethod(),
                              ),
                            );
                          },
                          child: Icon(
                            Icons.check_circle,
                            color: Colors.white,
                            size: 26,
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                SizedBox(
                  height: 10,
                ),
                Row(
                  children: [
                    CircleAvatar(
                      child: Icon(Icons.person),
                    ),
                    SizedBox(
                      width: 10,
                    ),
                    widget.name == null
                        ? FutureBuilder(
                            future: FirebaseFirestore.instance.collection("Users").doc(widget.otherId).get(),
                            builder: (context, snapshot) {
                              if (snapshot.hasData) {
                                print("other user id");
                                print(widget.otherId);
                                DocumentSnapshot doc = snapshot.data! as DocumentSnapshot;
                                print(doc.data());
                                AppUser otherUser = AppUser.fromJson(doc.data() as Map<String, dynamic>);

                                return Text(
                                  otherUser.getFullName(),
                                  style: TextStyle(fontSize: 22),
                                );
                              }
                              return Container(
                                  height: 10,
                                  width: 30,
                                  child: Center(
                                    child: LinearProgressIndicator(),
                                  ));
                            },
                          )
                        : Text(
                            widget.name,
                            style: TextStyle(fontSize: 22),
                          ),
                    Spacer(),
                    Icon(
                      Icons.phone,
                      color: Colors.white,
                    ),
                    Icon(
                      Icons.more_vert,
                      color: Colors.white,
                    ),
                  ],
                ),
                SizedBox(
                  height: 20,
                ),
                Expanded(
                    child: StreamBuilder<QuerySnapshot<Map<String, dynamic>>>(
                        stream: FirebaseFirestore.instance
                            .collection('Users')
                            .doc(FirestoreRepository.instance.currentUser.id)
                            .collection("Messages")
                            .where("otherId", isEqualTo: widget.otherId)
                            .orderBy("createdAt")
                            .snapshots(),
                        builder: (context, snapshot) {
                          if (!snapshot.hasData) {
                            return Center(
                              child: CircularProgressIndicator(),
                            );
                          }
                          List<Message> messages = [];
                          if (!snapshot.data!.docs.isEmpty) {
                            snapshot.data!.docs.forEach((element) {
                              messages.add(Message.fromJson(element.data()));
                            });
                          }
                          return ListView.builder(
                            itemCount: messages.length,
                            itemBuilder: (context, index) {
                              print(messages[index].toJson());
                              return MessageBubble(
                                sender: messages[index].isSender! ? "me" : "other",
                                text: messages[index].text ?? "content",
                                isMe: messages[index].isSender!,
                                imageUrl: messages[index].imageUrl,
                              );
                            },
                          );
                        })),
                Padding(
                  padding: EdgeInsets.symmetric(horizontal: 0.w, vertical: 10.h),
                  child: Container(
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(10),
                      // color: Colors.yellow[300],
                      color: kGreenCOLOR.withAlpha(100),
                    ),
                    child: ListTile(
                      leading: Transform.rotate(
                        angle: 45,
                        child: GestureDetector(
                            onTap: () async {
                              /*   image = await showDialog(
                      builder: (context) => const ShowImageDialog(),
                      context: context,
                    );
                    if (image != null) {
                      ifImage = true;
                      String? cloudStoragePath;
                      setState(() {
                        imageUploading = true;
                      });
                      cloudStoragePath = await FirestoreRepository.instance
                          .sendImage(
                          filePath: image!.path, fileName: image!.name);
                      setState(() {
                        imageUploading = true;
                      });
                      if (cloudStoragePath != null) {
                        FirestoreRepository.instance.sendMessage(
                            message: _textFieldController.text,
                            receiverId: widget.otherUserId,
                            receiverName: 'receiver',
                            imageUrl: cloudStoragePath);
                      }
                    }*/
                            },
                            child: const Icon(Icons.attach_file)),
                      ),
                      title: Expanded(
                        child: TextField(
                          controller: _textFieldController,
                          decoration: const InputDecoration.collapsed(hintText: 'send a message'),
                        ),
                      ),
                      trailing: GestureDetector(
                        onTap: () async {
                          print(FirestoreRepository.instance.currentUser.id);
                          if (_textFieldController.text.isNotEmpty) {
                            FirestoreRepository.instance.sendMessage(
                                message: _textFieldController.text,
                                receiverId: widget.otherId,
                                receiverName: 'receiver',
                                imageUrl: '');
                            _textFieldController.clear();
                          }
                        },
                        child: Transform.rotate(
                          angle: -45,
                          child: const Icon(Icons.send_rounded),
                        ),
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
