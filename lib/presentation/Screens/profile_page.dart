import 'package:co_services_students/core/image_dialog.dart';
import 'package:co_services_students/data/repositories/cloud_storage_repository.dart';
import 'package:co_services_students/data/repositories/firestore_repository.dart';
import 'package:co_services_students/logic/cubit/navigation_cubit.dart';
import 'package:co_services_students/presentation/Screens/authentication/landing_screen.dart';
import 'package:co_services_students/presentation/Screens/authentication/store.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:image_picker/image_picker.dart';

import '../../core/main_constant.dart';

class ProfilePage extends StatefulWidget {
  const ProfilePage({Key? key}) : super(key: key);

  @override
  State<ProfilePage> createState() => _ProfilePageState();
}

class _ProfilePageState extends State<ProfilePage> {
  bool? value1 = FirestoreRepository.instance.currentUser.fieldList!.contains(Store.category[0]);
  bool? value2 = FirestoreRepository.instance.currentUser.fieldList!.contains(Store.category[1]);
  bool? value3 = FirestoreRepository.instance.currentUser.fieldList!.contains(Store.category[2]);
  bool? value4 = FirestoreRepository.instance.currentUser.fieldList!.contains(Store.category[3]);

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: Container(
          decoration: const BoxDecoration(
            image: DecorationImage(
              image: AssetImage("assets/images/tob.jpg"),
              fit: BoxFit.cover,
            ),
          ),
          child: Padding(
            padding: EdgeInsets.only(left: 20.w, right: 20.w),
            child: SingleChildScrollView(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  SizedBox(
                    width: getScreenWidth(context),
                    height: 50.h,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        GestureDetector(
                          onTap: () {
                            context.read<NavigationCubit>().popFromHome();
                          },
                          child: Row(
                            children: [
                              const Icon(
                                Icons.arrow_back_ios_outlined,
                                color: Colors.white70,
                              ),
                              Text(
                                "Back",
                                style: TextStyle(color: Colors.white70, fontSize: 20.sp),
                              )
                            ],
                          ),
                        ),
                        TextButton(
                            onPressed: () async {
                              //TODO: update FirestoreRepository.currentUser object before
                              // example FirestoreRepository.instance.currentUser.firstName = " ";
                              List<String> newFields = [];
                              if (value1!) {
                                newFields.add(Store.category[0]);
                              }
                              if (value2!) {
                                newFields.add(Store.category[1]);
                              }
                              if (value3!) {
                                newFields.add(Store.category[2]);
                              }
                              if (value4!) {
                                newFields.add(Store.category[3]);
                              }
                              FirestoreRepository.instance.currentUser.fieldList = newFields;
                              await FirestoreRepository.instance.saveUserInfo();
                              context.read<NavigationCubit>().popFromHome();
                            },
                            child: Text(
                              "Save",
                              style: TextStyle(fontSize: 25.sp, color: Colors.white70),
                            ))
                      ],
                    ),
                  ),
                  SizedBox(
                    height: 25.h,
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        "My",
                        style: TextStyle(fontSize: 30.sp),
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text(
                            "Profile",
                            style: TextStyle(fontSize: 35.sp, fontWeight: FontWeight.bold),
                          ),
                          GestureDetector(
                              onTap: () {
                                FirebaseAuth.instance.signOut();
                                Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                    builder: (context) => LandingScreen(),
                                  ),
                                );
                              },
                              child: Icon(Icons.exit_to_app)),
                          Container(
                            decoration: BoxDecoration(
                              border: Border.all(
                                color: Colors.black87,
                                width: 2.w,
                              ),
                              borderRadius: const BorderRadius.all(Radius.circular(50)),
                            ),
                            child: FirebaseAuth.instance.currentUser!.photoURL != null
                                ? GestureDetector(
                                    onTap: changeProfilePhoto,
                                    child: Container(
                                      width: 50.sp,
                                      height: 50.sp,
                                      child: ClipRRect(
                                        borderRadius: const BorderRadius.all(Radius.circular(50)),
                                        child: Image.network(
                                          FirebaseAuth.instance.currentUser!.photoURL!,
                                          fit: BoxFit.cover,
                                        ),
                                      ),
                                    ))
                                : IconButton(
                                    iconSize: 50.sp,
                                    color: Colors.black87,
                                    onPressed: changeProfilePhoto,
                                    icon: const Icon(Icons.person)),
                          ),
                        ],
                      ),
                    ],
                  ),
                  Column(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            "First name",
                            style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18.sp),
                          ),
                          TextFormField(
                            enabled: false,
                            decoration: InputDecoration(
                              isDense: true,
                              fillColor: Colors.white,
                              hintText: FirestoreRepository.instance.currentUser.firstName,
                              hintStyle: TextStyle(color: Colors.grey, fontSize: 14.sp),
                              //errorText: " ",
                            ),
                            onChanged: (value) {},
                          ),
                        ],
                      ),
                      SizedBox(height: 15.h),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            "Last name",
                            style: TextStyle(
                              fontWeight: FontWeight.bold,
                              fontSize: 18.sp,
                            ),
                          ),
                          TextFormField(
                            enabled: false,
                            decoration: InputDecoration(
                              isDense: true,
                              fillColor: Colors.white,
                              hintText: FirestoreRepository.instance.currentUser.lastName,
                              hintStyle: TextStyle(color: Colors.grey, fontSize: 14.sp),
                              //errorText: " ",
                            ),
                            onChanged: (value) {},
                          ),
                        ],
                      ),
                      SizedBox(height: 15.h),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            "Email",
                            style: TextStyle(
                              fontWeight: FontWeight.bold,
                              fontSize: 18.sp,
                            ),
                          ),
                          TextFormField(
                            enabled: false,
                            decoration: InputDecoration(
                              isDense: true,
                              fillColor: Colors.white,
                              hintText: FirestoreRepository.instance.currentUser.email,
                              hintStyle: TextStyle(color: Colors.grey, fontSize: 14.sp),
                              //errorText: " ",
                            ),
                            onChanged: (value) {},
                          ),
                        ],
                      ),
                      SizedBox(height: 15.h),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            "Phone number",
                            style: TextStyle(
                              fontWeight: FontWeight.bold,
                              fontSize: 18.sp,
                            ),
                          ),
                          TextFormField(
                            enabled: false,
                            decoration: InputDecoration(
                              isDense: true,
                              fillColor: Colors.white,
                              hintText: FirestoreRepository.instance.currentUser.mobileNumber,
                              hintStyle: TextStyle(color: Colors.grey, fontSize: 14.sp),
                              //errorText: " ",
                            ),
                            onChanged: (value) {},
                          ),
                        ],
                      ),
                    ],
                  ),
                  Text(
                    "Field list",
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 22.sp,
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.only(top: 10.h, bottom: 10.h),
                    // height: 140.h,
                    width: getScreenWidth(context) - 100.w,
                    decoration: BoxDecoration(
                      color: Colors.white70,
                      boxShadow: [
                        BoxShadow(
                          color: Colors.grey.withOpacity(0.3),
                          spreadRadius: 2,
                          blurRadius: 7,
                          offset: const Offset(0, 0), // changes position of shadow
                        ),
                      ],
                      borderRadius: const BorderRadius.all(Radius.circular(25)),
                    ),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        SizedBox(height: 5.h),
                        Row(
                          children: [
                            Checkbox(
                              materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
                              value: value1,
                              activeColor: Colors.green,
                              onChanged: (bool? value) {
                                setState(() {
                                  value1 = value;
                                });
                              },
                            ),
                            Text(
                              Store.category[0],
                              style: TextStyle(color: Colors.blueGrey, fontSize: 14.sp),
                            )
                          ],
                        ),
                        Row(
                          children: [
                            Checkbox(
                              materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
                              value: value2,
                              activeColor: Colors.green,
                              onChanged: (bool? value) {
                                setState(() {
                                  value2 = value;
                                });
                              },
                            ),
                            Text(
                              Store.category[1],
                              style: TextStyle(color: Colors.blueGrey, fontSize: 14.sp),
                            )
                          ],
                        ),
                        Row(
                          children: [
                            Checkbox(
                              materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
                              value: value3,
                              activeColor: Colors.green,
                              onChanged: (bool? value) {
                                setState(() {
                                  value3 = value;
                                });
                              },
                            ),
                            Text(
                              Store.category[2],
                              style: TextStyle(color: Colors.blueGrey, fontSize: 14.sp),
                            )
                          ],
                        ),
                        Row(
                          children: [
                            Checkbox(
                              materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
                              value: value4,
                              activeColor: Colors.green,
                              onChanged: (bool? value) {
                                setState(() {
                                  value4 = value;
                                });
                              },
                            ),
                            Text(
                              Store.category[3],
                              style: TextStyle(color: Colors.blueGrey, fontSize: 14.sp),
                            )
                          ],
                        ),
                        SizedBox(height: 5.h),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  void changeProfilePhoto() async {
    XFile? image = await showDialog(
      builder: (context) => const ShowImageDialog(),
      context: context,
    );
    if (image == null) {
      return;
    }
    String? imageUrl;
    await showDialog(
      builder: (context) => AlertDialog(
        content: FutureBuilder(
          future:
              CloudStorageRepository.instance.uploadFile(filePath: image.path, fileName: "profilePhotos/${image.name}"),
          builder: (BuildContext context, AsyncSnapshot<String> snapshot) {
            if (snapshot.hasData) {
              print(snapshot.data!);
              return SizedBox(
                height: getScreenHeight(context) / 10,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    Text("Uploaded", style: TextStyle(fontSize: 25.sp, color: Colors.green)),
                    Center(
                      child: GestureDetector(
                          onTap: () {
                            print("snapshot data");
                            print(snapshot.data!);
                            imageUrl = snapshot.data!;
                            Navigator.pop(context);
                          },
                          child: Container(
                            padding: EdgeInsets.all(8.0),
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(10),
                              color: kGreenCOLOR,
                            ),
                            child: const Text(
                              "Confirm Changing Photo",
                              style: TextStyle(
                                color: Colors.white,
                              ),
                            ),
                          )),
                    ),
                  ],
                ),
              );
            } else {
              return SizedBox(
                height: getScreenHeight(context) / 10,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: const [
                    Text("Uploading"),
                    Center(
                      child: LinearProgressIndicator(),
                    ),
                  ],
                ),
              );
            }
          },
        ),
      ),
      context: context,
    );
    print("Image url is ");
    print(imageUrl);
    if (imageUrl != null) {
      showDialog(
        builder: (context) => AlertDialog(
          content: FutureBuilder(
            future: updateProfilePhoto(imageUrl!),
            builder: (BuildContext context, AsyncSnapshot<bool> snapshot) {
              if (snapshot.hasData) {
                return SizedBox(
                  height: getScreenHeight(context) / 10,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      Text("Updated", style: TextStyle(fontSize: 25.sp, color: Colors.green)),
                    ],
                  ),
                );
              } else {
                return SizedBox(
                  height: getScreenHeight(context) / 10,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: const [
                      Text("Updating profile photo"),
                      Center(
                        child: LinearProgressIndicator(),
                      ),
                    ],
                  ),
                );
              }
            },
          ),
        ),
        context: context,
      );
    }
  }

  Future<bool> updateProfilePhoto(String imageUrl) async {
    await FirebaseAuth.instance.currentUser?.updatePhotoURL(imageUrl);
    return true;
  }
}

/*
InkWell(
      child: Tab(
        icon: Image.asset(
          widget.iconPath,
          width: width,
          height: height,
        ),
        text: text,
      ),
      onTap: onTap,
    );
 */
