import 'package:co_services_students/data/models/service.dart';
import 'package:co_services_students/data/repositories/firestore_repository.dart';
import 'package:co_services_students/logic/cubit/navigation_cubit.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:image_picker/image_picker.dart';

import '../../core/image_dialog.dart';
import '../../core/main_constant.dart';
import '../../data/repositories/cloud_storage_repository.dart';
import 'authentication/store.dart';

class AddServiceScreen extends StatefulWidget {
  const AddServiceScreen({Key? key}) : super(key: key);

  @override
  State<AddServiceScreen> createState() => _AddServiceScreenState();
}

class _AddServiceScreenState extends State<AddServiceScreen> {
  bool? value1 = false;
  bool? value2 = false;
  bool? value3 = false;
  bool? value4 = false;
  bool? value5 = false;

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: SingleChildScrollView(
          child: Container(
            decoration: const BoxDecoration(
              image: DecorationImage(
                image: AssetImage("assets/images/tob.jpg"),
                fit: BoxFit.cover,
              ),
            ),
            child: Padding(
              padding: EdgeInsets.only(left: 20.w, right: 20.w),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  GestureDetector(
                    onTap: () {
                      context.read<NavigationCubit>().setIndex(0);
                    },
                    child: SizedBox(
                      width: getScreenWidth(context),
                      height: 50.h,
                      child: Row(
                        children: [
                          const Icon(
                            Icons.arrow_back_ios_outlined,
                            color: Colors.white70,
                          ),
                          Text(
                            "Back",
                            style: TextStyle(color: Colors.white70, fontSize: 20.sp),
                          )
                        ],
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 30.h,
                  ),
                  Text(
                    "Hi",
                    style: TextStyle(fontSize: 30.sp),
                  ),
                  SizedBox(
                    height: 5.h,
                  ),
                  Text(
                    FirestoreRepository.instance.currentUser.getFullName(),
                    style: TextStyle(fontSize: 28.sp, fontWeight: FontWeight.bold),
                  ),
                  Padding(
                    padding: EdgeInsets.only(top: 20.h, bottom: 10.h),
                    child: Container(
                      height: 125.h,
                      width: getScreenWidth(context) - 60.w,
                      decoration: BoxDecoration(
                        color: kGreyCOLOR,
                        boxShadow: [
                          BoxShadow(
                            color: Colors.grey.withOpacity(0.5),
                            spreadRadius: 1,
                            blurRadius: 2,
                            offset: const Offset(0, 0), // changes position of shadow
                          ),
                        ],
                        borderRadius: const BorderRadius.all(Radius.circular(40)),
                      ),
                      child: Padding(
                        padding: EdgeInsets.all(10.w),
                        child: TextFormField(
                          maxLines: 50,
                          minLines: 1,
                          style: TextStyle(fontSize: 18.sp),
                          decoration: InputDecoration(
                            hintStyle: TextStyle(fontSize: 16.sp),
                            hintText: "Explain what you offer..........",
                            fillColor: Colors.white,
                            border: InputBorder.none,
                          ),
                          keyboardType: TextInputType.multiline,
                          onChanged: (value) {
                            List<String> array = value.split('\n');
                            Store.header = array[0];
                            Store.content = value;
                          },
                        ),
                      ),
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.only(bottom: 10.h),
                    child: Container(
                      margin: EdgeInsets.only(top: 10.h, bottom: 10.h),
                      width: getScreenWidth(context) - 60.w,
                      decoration: BoxDecoration(
                        color: Colors.white,
                        boxShadow: [
                          BoxShadow(
                            color: Colors.grey.withOpacity(0.3),
                            spreadRadius: 2,
                            blurRadius: 7,
                            offset: const Offset(0, 0), // changes position of shadow
                          ),
                        ],
                        borderRadius: const BorderRadius.all(Radius.circular(25)),
                      ),
                      child: Column(
                        children: [
                          Row(
                            children: [
                              Checkbox(
                                materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
                                value: value1,
                                activeColor: Colors.green,
                                onChanged: (bool? value) {
                                  setState(() {
                                    value1 = value;
                                    Store.categories.add(Store.category[3]);
                                  });
                                },
                              ),
                              Text(
                                "Edit videos and photo",
                                style: TextStyle(color: Colors.blueGrey, fontSize: 14.sp),
                              )
                            ],
                          ),
                          Row(
                            children: [
                              Checkbox(
                                materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
                                value: value2,
                                activeColor: Colors.green,
                                onChanged: (bool? value) {
                                  setState(() {
                                    value2 = value;
                                    Store.categories.add(Store.category[0]);
                                  });
                                },
                              ),
                              Text(
                                "programming and development",
                                style: TextStyle(color: Colors.blueGrey, fontSize: 14.sp),
                              )
                            ],
                          ),
                          Row(
                            children: [
                              Checkbox(
                                materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
                                value: value3,
                                activeColor: Colors.green,
                                onChanged: (bool? value) {
                                  setState(() {
                                    value3 = value;
                                    Store.categories.add(Store.category[1]);
                                  });
                                },
                              ),
                              Text(
                                "writing and translation",
                                style: TextStyle(color: Colors.blueGrey, fontSize: 14.sp),
                              )
                            ],
                          ),
                          Row(
                            children: [
                              Checkbox(
                                materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
                                value: value4,
                                activeColor: Colors.green,
                                onChanged: (bool? value) {
                                  setState(() {
                                    value4 = value;
                                    Store.categories.add(Store.category[2]);
                                  });
                                },
                              ),
                              Text(
                                "distance courses and explanations",
                                style: TextStyle(color: Colors.blueGrey, fontSize: 14.sp),
                              )
                            ],
                          ),
                          Row(
                            children: [
                              Checkbox(
                                materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
                                value: value5,
                                activeColor: Colors.green,
                                onChanged: (bool? value) {
                                  setState(() {
                                    value5 = value;
                                    Store.categories.add(Store.category[4]);
                                  });
                                },
                              ),
                              Text(
                                "view items for sell",
                                style: TextStyle(color: Colors.blueGrey, fontSize: 14.sp),
                              )
                            ],
                          ),
                        ],
                      ),
                    ),
                  ),
                  Center(
                    child: Column(
                      children: [
                        Container(
                          height: 45.h,
                          width: getScreenWidth(context) - 130.w,
                          decoration: const BoxDecoration(
                              borderRadius: BorderRadius.all(
                                Radius.circular(40),
                              ),
                              color: kBlackColor),
                          child: TextButton(
                              onPressed: () async {
                                XFile? image = await showDialog(
                                  builder: (context) => const ShowImageDialog(),
                                  context: context,
                                );
                                if (image != null) {
                                  showDialog(
                                    builder: (context) => AlertDialog(
                                      content: FutureBuilder(
                                        future: CloudStorageRepository.instance
                                            .uploadFile(filePath: image.path, fileName: image.name),
                                        builder: (BuildContext context, AsyncSnapshot<String> snapshot) {
                                          if (snapshot.hasData) {
                                            Store.imageUrl = snapshot.data;
                                            print(snapshot.data!);
                                            return SizedBox(
                                              height: getScreenHeight(context) / 10,
                                              child: Column(
                                                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                                children: [
                                                  Text("Uploaded",
                                                      style: TextStyle(fontSize: 25.sp, color: Colors.green)),
                                                  Center(
                                                    child: GestureDetector(
                                                        onTap: () {
                                                          Navigator.pop(context);
                                                        },
                                                        child: Text("Done")),
                                                  ),
                                                ],
                                              ),
                                            );
                                          } else {
                                            return SizedBox(
                                              height: getScreenHeight(context) / 10,
                                              child: Column(
                                                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                                children: const [
                                                  Text("Uploading"),
                                                  Center(
                                                    child: LinearProgressIndicator(),
                                                  ),
                                                ],
                                              ),
                                            );
                                          }
                                        },
                                      ),
                                    ),
                                    context: context,
                                  );
                                }
                              },
                              child: Text(
                                "Upload Image",
                                style: TextStyle(color: Colors.white, fontSize: 18.sp),
                              )),
                        ),
                        SizedBox(height: 15.h),
                        Container(
                          height: 45.h,
                          width: getScreenWidth(context) - 130.w,
                          decoration: const BoxDecoration(
                            borderRadius: BorderRadius.all(Radius.circular(40.0)),
                            color: kBlackColor,
                          ),
                          child: TextButton(
                            onPressed: () async {
                              Service service = Store.getService();
                              print(service.toJson());
                              showDialog(
                                builder: (context) => AlertDialog(
                                  content: FutureBuilder(
                                    future: FirestoreRepository.instance.postService(service),
                                    builder: (context, snapshot) {
                                      if (snapshot.hasData) {
                                        return SizedBox(
                                          height: getScreenHeight(context) / 10,
                                          child: Column(
                                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                            children: [
                                              Text("Posted", style: TextStyle(fontSize: 25.sp, color: Colors.green)),
                                              Center(
                                                child: GestureDetector(
                                                    onTap: () {
                                                      Navigator.pop(context);
                                                    },
                                                    child: Text("Done")),
                                              ),
                                            ],
                                          ),
                                        );
                                      } else {
                                        return SizedBox(
                                          height: getScreenHeight(context) / 10,
                                          child: Column(
                                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                            children: const [
                                              Text("Posting"),
                                              Center(
                                                child: LinearProgressIndicator(),
                                              ),
                                            ],
                                          ),
                                        );
                                      }
                                    },
                                  ),
                                ),
                                context: context,
                              );
                              Store.cleanServiceStore();
                              print("added");
                            },
                            child: Text(
                              "Post",
                              style: TextStyle(color: Colors.white, fontSize: 18.sp),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
