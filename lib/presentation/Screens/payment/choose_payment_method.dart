import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import '../../../core/main_constant.dart';
import 'master_card_payment.dart';

class ChoosePaymentMethod extends StatelessWidget {
  const ChoosePaymentMethod({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: Container(
          decoration: const BoxDecoration(
            image: DecorationImage(
              image: AssetImage("assets/images/tob.jpg"),
              fit: BoxFit.cover,
            ),
          ),
          child: Padding(
            padding: EdgeInsets.only(left: 25.w, right: 25.w),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                GestureDetector(
                  onTap: () {},
                  child: SizedBox(
                    width: getScreenWidth(context),
                    height: 50.h,
                    child: Row(
                      children: [
                        const Icon(
                          Icons.arrow_back_ios_outlined,
                          color: Colors.white70,
                        ),
                        Text(
                          "Back",
                          style: TextStyle(color: Colors.white70, fontSize: 20.sp),
                        ),
                      ],
                    ),
                  ),
                ),
                SizedBox(
                  height: 80.h,
                ),
                Text(
                  "choose payment",
                  style: TextStyle(fontSize: 24.sp, fontWeight: FontWeight.bold),
                ),
                Text(
                  "method",
                  style: TextStyle(fontSize: 24.sp, fontWeight: FontWeight.bold),
                ),
                const Spacer(flex: 2),
                Column(
                  children: [
                    GestureDetector(
                      onTap: () {
                        Navigator.pushReplacement(
                          context,
                          MaterialPageRoute(
                            builder: (context) => MasterCardPayment(),
                          ),
                        );
                      },
                      child: Row(
                        children: [
                          Container(
                            height: 40.h,
                            width: getScreenWidth(context) * 4 / 6,
                            decoration: const BoxDecoration(
                              borderRadius:
                                  BorderRadius.only(topLeft: Radius.circular(10.0), bottomLeft: Radius.circular(10.0)),
                              color: kGreyCOLOR,
                            ),
                            child: Center(child: Text("Payment when receiving", style: TextStyle(fontSize: 18.sp))),
                          ),
                          Container(
                              height: 40.h,
                              width: getScreenWidth(context) * 1 / 6,
                              decoration: const BoxDecoration(
                                borderRadius: BorderRadius.only(
                                    bottomRight: Radius.circular(10.0), topRight: Radius.circular(10.0)),
                                color: kGreenCOLOR,
                              ),
                              child: Icon(
                                Icons.arrow_right_alt_sharp,
                                size: 40.sp,
                                color: Colors.white,
                              ))
                        ],
                      ),
                    ),
                    SizedBox(height: 15.h),
                    GestureDetector(
                      onTap: () {
                        Navigator.pushReplacement(
                          context,
                          MaterialPageRoute(
                            builder: (context) => MasterCardPayment(),
                          ),
                        );
                      },
                      child: Row(
                        children: [
                          Container(
                            height: 40.h,
                            width: getScreenWidth(context) * 4 / 6,
                            decoration: const BoxDecoration(
                              borderRadius:
                                  BorderRadius.only(topLeft: Radius.circular(10.0), bottomLeft: Radius.circular(10.0)),
                              color: kGreyCOLOR,
                            ),
                            child: Center(child: Text("Online payment", style: TextStyle(fontSize: 18.sp))),
                          ),
                          Container(
                              height: 40.h,
                              width: getScreenWidth(context) * 1 / 6,
                              decoration: const BoxDecoration(
                                borderRadius: BorderRadius.only(
                                    bottomRight: Radius.circular(10.0), topRight: Radius.circular(10.0)),
                                color: kGreenCOLOR,
                              ),
                              child: Icon(
                                Icons.arrow_right_alt_sharp,
                                size: 40.sp,
                                color: Colors.white,
                              ))
                        ],
                      ),
                    )
                  ],
                ),
                const Spacer(),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
