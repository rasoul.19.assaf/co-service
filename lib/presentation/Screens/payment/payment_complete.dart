import 'package:co_services_students/presentation/Screens/home_screen.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import '../../../core/main_constant.dart';
import '../../../logic/cubit/navigation_cubit.dart';

class PaymentComplete extends StatelessWidget {
  String holderName;

  PaymentComplete({required this.holderName});

  @override
  Widget build(BuildContext context) {
    print("Building ");
    print(holderName);
    return SafeArea(
      child: Scaffold(
        body: Container(
          decoration: const BoxDecoration(
            image: DecorationImage(
              image: AssetImage("assets/images/tob.jpg"),
              fit: BoxFit.cover,
            ),
          ),
          child: Padding(
            padding: EdgeInsets.only(left: 40.w, right: 40.w),
            child: SingleChildScrollView(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  GestureDetector(
                    onTap: () {
                      Navigator.pop(context);
                    },
                    child: SizedBox(
                      width: getScreenWidth(context),
                      height: 50.h,
                      child: Row(
                        children: [
                          const Icon(
                            Icons.arrow_back_ios_outlined,
                            color: Colors.white70,
                          ),
                          Text(
                            "Back",
                            style: TextStyle(color: Colors.white70, fontSize: 20.sp),
                          ),
                        ],
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 60.h,
                  ),
                  Center(
                    child: SizedBox(
                      height: 150.h,
                      width: 160.w,
                      child: Image.asset(
                        "assets/images/complete.png",
                        fit: BoxFit.cover,
                      ),
                    ),
                  ),
                  Center(
                    child: Padding(
                      padding: EdgeInsets.only(top: 25.w, bottom: 25.w),
                      child: Column(
                        children: [
                          Text(
                            "Payment Complete",
                            style: TextStyle(fontSize: 23.sp, fontWeight: FontWeight.bold),
                          ),
                          SizedBox(
                            height: 15.h,
                          ),
                          Text(
                            "Thank you, your payment has been",
                            style: TextStyle(fontSize: 17.sp),
                          ),
                          Text(
                            "successful. A confirmation email has",
                            style: TextStyle(fontSize: 17.sp),
                          ),
                          Text(
                            "been sent to matt@example.com",
                            style: TextStyle(fontSize: 17.sp),
                          ),
                        ],
                      ),
                    ),
                  ),
                  Text("ORDER DETAILS", style: TextStyle(fontSize: 14.sp, fontWeight: FontWeight.bold)),
                  Padding(
                    padding: EdgeInsets.only(top: 5.h, bottom: 5.h),
                    child: const Divider(
                      thickness: 2,
                    ),
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          Text(
                            "ORDER REF",
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                          Text("8675309"),
                        ],
                      ),
                      SizedBox(height: 10.h),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          Text(
                            "ORDER DATE",
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                          Text("Dec 7 , 2015,11:52"),
                        ],
                      ),
                      SizedBox(height: 10.h),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          Text(
                            "PAYMENT TYPE",
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                          Text("Visa"),
                        ],
                      ),
                      SizedBox(height: 10.h),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          Text(
                            "CARDHOLDERS NAME",
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                          Text(holderName),
                        ],
                      ),
                    ],
                  ),
                  SizedBox(height: 40.h),
                  Center(
                    child: Container(
                      height: 45.h,
                      width: getScreenWidth(context) - 120.w,
                      decoration: const BoxDecoration(
                        borderRadius: BorderRadius.all(Radius.circular(40.0)),
                        color: kBlackColor,
                      ),
                      child: TextButton(
                        onPressed: () {
                          Navigator.pushReplacement(
                            context,
                            MaterialPageRoute(
                              builder: (context) => BlocProvider(
                                create: (context) => NavigationCubit(),
                                child: HomeScreen(),
                              ),
                            ),
                          );
                        },
                        child: Text(
                          "BACK TO HOME PAGE",
                          style: TextStyle(color: Colors.white, fontSize: 18.sp),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
