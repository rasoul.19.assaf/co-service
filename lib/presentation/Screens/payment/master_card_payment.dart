import 'package:co_services_students/presentation/Screens/payment/payment_complete.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import '../../../core/main_constant.dart';

class MasterCardPayment extends StatefulWidget {
  MasterCardPayment({Key? key}) : super(key: key);

  @override
  State<MasterCardPayment> createState() => _MasterCardPaymentState();
}

class _MasterCardPaymentState extends State<MasterCardPayment> {
  String holderName = "";

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: Container(
          decoration: const BoxDecoration(
            image: DecorationImage(
              image: AssetImage("assets/images/tob.jpg"),
              fit: BoxFit.cover,
            ),
          ),
          child: Padding(
            padding: EdgeInsets.only(left: 25.w, right: 25.w),
            child: SingleChildScrollView(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  SizedBox(
                    height: 50.h,
                    width: getScreenWidth(context),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        GestureDetector(
                          onTap: () {},
                          child: Row(
                            children: [
                              Icon(
                                Icons.home_filled,
                                size: 40.sp,
                                color: Colors.white70,
                              ),
                            ],
                          ),
                        ),
                        GestureDetector(
                          onTap: () {},
                          child: Row(
                            children: [
                              Icon(
                                Icons.subdirectory_arrow_left,
                                size: 30.sp,
                                color: Colors.white70,
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                  SizedBox(
                    height: 80.h,
                  ),
                  Column(
                    children: [
                      Container(
                        height: 160.h,
                        width: getScreenWidth(context),
                        decoration: const BoxDecoration(
                          borderRadius: BorderRadius.all(Radius.circular(10.0)),
                          color: kGreenCOLOR,
                        ),
                        child: Padding(
                          padding: EdgeInsets.all(20.w),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Row(
                                children: [
                                  SizedBox(
                                    height: 30.h,
                                    width: 60.w,
                                    child: Image.asset(
                                      "assets/images/mastercard.png",
                                      fit: BoxFit.cover,
                                    ),
                                  ),
                                  SizedBox(width: 15.w),
                                  Text("MasterCard")
                                ],
                              ),
                              Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: [
                                  Text(
                                    "7364 1835 5532 2233",
                                    style: TextStyle(color: Colors.white, fontSize: 16.sp),
                                  ),
                                  Text(
                                    "01/20",
                                    style: TextStyle(color: Colors.white, fontSize: 16.sp),
                                  )
                                ],
                              ),
                            ],
                          ),
                        ),
                      ),
                      SizedBox(height: 20.h),
                      Container(
                        height: 55.h,
                        width: getScreenWidth(context),
                        decoration: const BoxDecoration(
                          borderRadius: BorderRadius.all(Radius.circular(10.0)),
                          color: kLightGreenCOLOR,
                        ),
                        child: Padding(
                          padding: EdgeInsets.only(left: 25.w),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                "Card holder name",
                                style: TextStyle(color: Colors.white70, fontSize: 16.sp),
                              ),
                              TextField(
                                decoration: InputDecoration(
                                  isDense: true,
                                  border: InputBorder.none,
                                ),
                                onChanged: (value) {
                                  setState(() {
                                    holderName = value;
                                  });
                                },
                                style: TextStyle(color: Colors.white, fontSize: 18.sp),
                              ),
                            ],
                          ),
                        ),
                      ),
                      SizedBox(height: 15.h),
                      Container(
                        height: 55.h,
                        width: getScreenWidth(context),
                        decoration: const BoxDecoration(
                          borderRadius: BorderRadius.all(Radius.circular(10.0)),
                          color: kLightGreenCOLOR,
                        ),
                        child: Padding(
                          padding: EdgeInsets.only(left: 25.w),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                "Card number",
                                style: TextStyle(color: Colors.white70, fontSize: 16.sp),
                              ),
                              TextField(
                                decoration: InputDecoration(
                                  isDense: true,
                                  border: InputBorder.none,
                                ),
                                style: TextStyle(color: Colors.white, fontSize: 18.sp),
                              ),
                            ],
                          ),
                        ),
                      ),
                      SizedBox(height: 15.h),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Container(
                            height: 55.h,
                            width: getScreenWidth(context) / 2.5,
                            decoration: const BoxDecoration(
                              borderRadius: BorderRadius.all(Radius.circular(10.0)),
                              color: kLightGreenCOLOR,
                            ),
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                              children: [
                                Text(
                                  "Expiration date",
                                  style: TextStyle(color: Colors.white70, fontSize: 16.sp),
                                ),
                                TextField(
                                  decoration: InputDecoration(
                                    isDense: true,
                                    border: InputBorder.none,
                                  ),
                                  style: TextStyle(color: Colors.white, fontSize: 18.sp),
                                ),
                              ],
                            ),
                          ),
                          Container(
                            height: 55.h,
                            width: getScreenWidth(context) / 2.5,
                            decoration: const BoxDecoration(
                              borderRadius: BorderRadius.all(Radius.circular(10.0)),
                              color: kLightGreenCOLOR,
                            ),
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                              children: [
                                Text(
                                  "CVV",
                                  style: TextStyle(color: Colors.white70, fontSize: 16.sp),
                                ),
                                TextField(
                                  decoration: InputDecoration(
                                    isDense: true,
                                    border: InputBorder.none,
                                  ),
                                  style: TextStyle(color: Colors.white, fontSize: 18.sp),
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                      SizedBox(height: 20.h),
                      Container(
                        height: 45.h,
                        width: getScreenWidth(context) - 150.w,
                        decoration: const BoxDecoration(
                          borderRadius: BorderRadius.all(Radius.circular(40.0)),
                          color: kBlackColor,
                        ),
                        child: TextButton(
                          onPressed: () {
                            Navigator.pushReplacement(
                              context,
                              MaterialPageRoute(builder: (context) {
                                return PaymentComplete(
                                  holderName: holderName,
                                );
                              }),
                            );
                          },
                          child: Text(
                            "Pay",
                            style: TextStyle(color: Colors.white, fontSize: 18.sp),
                          ),
                        ),
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 10.h,
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
