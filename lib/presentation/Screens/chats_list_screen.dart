import 'package:co_services_students/data/models/app_user.dart';
import 'package:co_services_students/data/repositories/firestore_repository.dart';
import 'package:co_services_students/presentation/Screens/chatting_screen.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class ChatsListScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: const BoxDecoration(
        image: DecorationImage(
          image: AssetImage("assets/images/tob.jpg"),
          fit: BoxFit.cover,
        ),
      ),
      child: Padding(
        padding: EdgeInsets.only(left: 5.w, right: 5.w),
        child: Column(
          children: [
            Align(
              alignment: Alignment.centerLeft,
              child: Padding(
                padding: EdgeInsets.all(12),
                child: Text(
                  "Your Chats",
                  style: TextStyle(color: Colors.white, fontSize: 24.sp),
                ),
              ),
            ),
            SizedBox(
              height: 30.h,
            ),
            FutureBuilder(
              builder: (context, snapshot) {
                if (!snapshot.hasData) {
                  return Center(child: CircularProgressIndicator());
                } else {
                  List<AppUser> users = snapshot.data! as List<AppUser>;
                  print(FirestoreRepository.instance.currentUser.toJson());
                  return Expanded(
                    child: ListView.builder(
                      itemCount: users.length,
                      itemBuilder: (context, index) {
                        return GestureDetector(
                          onTap: () {
                            Navigator.push(
                              context,
                              MaterialPageRoute(
                                builder: (context) =>
                                    ChattingScreen(otherId: users[index].id, name: users[index].getFullName()),
                              ),
                            );
                          },
                          child: ListTile(
                            leading: CircleAvatar(),
                            title: Text("${users[index].firstName} ${users[index].lastName}"),
                            trailing: Text("date-time"),
                          ),
                        );
                      },
                    ),
                  );
                }
              },
              future: FirestoreRepository.instance.getContacts(),
            )
          ],
        ),
      ),
    );
  }
}
