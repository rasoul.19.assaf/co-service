import 'package:co_services_students/core/main_constant.dart';
import 'package:co_services_students/logic/cubit/navigation_cubit.dart';
import 'package:co_services_students/presentation/widgets/my_category_button.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class CategoriesScreen extends StatelessWidget {
  final Future<bool> Function()? onWillPop;

  const CategoriesScreen({Key? key, required this.onWillPop}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: onWillPop,
      child: Container(
          decoration: const BoxDecoration(
            image: DecorationImage(
              image: AssetImage("assets/images/tob.jpg"),
              fit: BoxFit.cover,
            ),
          ),
          child: Padding(
            padding: EdgeInsets.only(left: 20.w, right: 20.w),
            child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
              GestureDetector(
                onTap: () {
                  context.read<NavigationCubit>().setIndex(0);
                },
                child: SizedBox(
                  width: getScreenWidth(context),
                  height: 50.h,
                  child: Row(
                    children: [
                      const Icon(
                        Icons.arrow_back_ios_outlined,
                        color: Colors.white70,
                      ),
                      Text(
                        "Back",
                        style: TextStyle(color: Colors.white70, fontSize: 20.sp),
                      )
                    ],
                  ),
                ),
              ),
              Expanded(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children:  [
                    MyCategoryButton(
                      category: "programming and development",
                      title: 'Programming and Development',
                      icon: Icons.computer,
                    ),
                    SizedBox(
                      height: 10.h,
                    ),
                    MyCategoryButton(
                      category: "writing and translation",
                      title: 'Writing and translation',
                      icon: Icons.translate,
                    ),
                    SizedBox(
                      height: 10.h,
                    ),
                    MyCategoryButton(
                      category: "distance courses and explainations",
                      title: 'Distance courses and explainations',
                      icon: Icons.menu_book_sharp,
                    ),
                    SizedBox(
                      height: 10.h,
                    ),
                    MyCategoryButton(
                      category: "edit videos and photos",
                      title: 'Edit videos and photos',
                      icon: Icons.edit,
                    ),
                    SizedBox(
                      height: 10.h,
                    ),
                    MyCategoryButton(
                      category: "view items for sell",
                      title: 'View Items for Sell',
                      icon: Icons.sell,
                    ),
                    SizedBox(
                      height: 10.h,
                    ),
                  ],
                ),
              )
            ]),
          )),
    );
  }
}
