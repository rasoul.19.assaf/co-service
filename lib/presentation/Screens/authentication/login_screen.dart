import 'package:co_services_students/core/main_constant.dart';
import 'package:co_services_students/data/repositories/firestore_repository.dart';
import 'package:co_services_students/logic/cubit/navigation_cubit.dart';
import 'package:co_services_students/presentation/Screens/authentication/signup_screen.dart';
import 'package:co_services_students/presentation/Screens/authentication/store.dart';
import 'package:co_services_students/presentation/Screens/home_screen.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class SignInScreen extends StatelessWidget {
  const SignInScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        //TODO add back icon
        body: Container(
          decoration: const BoxDecoration(
            image: DecorationImage(
              image: AssetImage("assets/images/tob.jpg"),
              fit: BoxFit.cover,
            ),
          ),
          child: Padding(
            padding: EdgeInsets.only(left: 25.w, right: 25.w),
            child: SingleChildScrollView(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  SizedBox(
                    height: 100.h,
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        "Account",
                        style: TextStyle(fontSize: 35.sp),
                      ),
                      Text(
                        "Login",
                        style: kh1TextStyle,
                      ),
                      Padding(
                        padding: EdgeInsets.only(top: 20.h),
                        child: Text(
                          "All fields are required",
                          style: TextStyle(fontSize: 25.sp, color: Colors.blueGrey),
                        ),
                      ),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        "Email",
                        style: kTextFieldLabelTextStyle,
                      ),
                      // SizedBox(height: 10.h),
                      TextFormField(
                        decoration: InputDecoration(
                          isDense: true,
                          fillColor: Colors.white,
                          hintText: "enter your email",
                          hintStyle: TextStyle(color: Colors.grey, fontSize: 18.sp),
                          //errorText: " ",
                        ),
                        onChanged: (value) {
                          Store.email = value;
                        },
                      ),
                      SizedBox(height: 30.h),
                      Text(
                        "Password",
                        style: kTextFieldLabelTextStyle,
                      ),
                      TextFormField(
                        decoration: InputDecoration(
                          isDense: true,
                          fillColor: Colors.white,
                          hintText: "enter your password",
                          hintStyle: TextStyle(color: Colors.grey, fontSize: 18.sp),
                          //errorText: " ",
                        ),
                        onChanged: (value) {
                          Store.password = value;
                        },
                      ),
                    ],
                  ),
                  Center(
                    child: Padding(
                      padding: EdgeInsets.only(bottom: 20.h),
                      child: Column(
                        children: [
                          SizedBox(
                            height: 20,
                          ),
                          SizedBox(
                            width: getScreenWidth(context) - 120.w,
                            child: ElevatedButton(
                              child: Text(
                                'LOGIN',
                                style: TextStyle(color: Colors.white, fontSize: 18.sp),
                              ),
                              onPressed: () async {
                                UserCredential? result;
                                if (Store.password!.isNotEmpty && Store.email!.isNotEmpty) {
                                  try {
                                    result = await FirebaseAuth.instance.signInWithEmailAndPassword(
                                      email: Store.email!,
                                      password: Store.password!,
                                    );
                                    await FirestoreRepository.instance.initCurrentUser(result.user!);

                                    Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) {
                                      return BlocProvider(
                                        create: (context) => NavigationCubit(),
                                        child: HomeScreen(),
                                      );
                                    }));
                                  } catch (e) {
                                    print("error");
                                    print(e);
                                    showDialog(
                                      builder: (context) => AlertDialog(
                                        content: Column(
                                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                          children: [
                                            Text("Error", style: TextStyle(fontSize: 25.sp, color: Colors.red)),
                                            Center(
                                              child: Text("${result!.credential}"),
                                            ),
                                          ],
                                        ),
                                      ),
                                      context: context,
                                    );
                                  }
                                } else {
                                  showDialog(
                                    builder: (context) => AlertDialog(
                                      content: Column(
                                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                        children: [
                                          Text("Error", style: TextStyle(fontSize: 25.sp, color: Colors.red)),
                                          const Center(
                                            child: Text("Wrong Inputs"),
                                          ),
                                        ],
                                      ),
                                    ),
                                    context: context,
                                  );
                                }
                              },
                              style: kButtonStyleSecondary,
                            ),
                          ),
                          SizedBox(height: 15.h),
                          Container(
                            height: 50.h,
                            width: getScreenWidth(context) - 120.w,
                            decoration: const BoxDecoration(
                              borderRadius: BorderRadius.all(Radius.circular(40.0)),
                              color: kBlackColor,
                            ),
                            child: TextButton(
                              onPressed: () {
                                Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) {
                                  return SignUpScreen();
                                }));
                              },
                              child: Text(
                                "CREATE ACCOUNT",
                                style: TextStyle(color: Colors.white, fontSize: 18.sp),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
