import '../../../data/models/app_user.dart';
import '../../../data/models/service.dart';

class Store {
  /// User
  static String? userId;
  static String? firstName;
  static String? lastName;
  static List<String>? contactsIds;
  static String? email;
  static String? mobileNumber;
  static List<String>? fieldList;
  static String? password;

  /// Service
  static String? serviceId;
  static String? header;
  static String? content;
  static String? imageUrl;
  static List<String> categories = [];
  static String? providerId;

  static List<String> category = [
    "programming and development",
    "writing and translation",
    "distance courses and explainations",
    "edit videos and photos",
    "view items for sell"
  ];

  static Service getService() {
    Service service =
        Service(header: header, content: content, imageUrl: imageUrl, categories: categories, providerId: userId);
    return service;
  }

  static cleanServiceStore() {
    serviceId = null;
    header = null;
    content = null;
    imageUrl = null;
    categories = [];
    providerId = null;
  }

  static bool validateInputs() {
    if (email!.isNotEmpty &&
        password!.isNotEmpty &&
        mobileNumber!.isNotEmpty &&
        firstName!.isNotEmpty &&
        lastName!.isNotEmpty) {
      return true;
    } else {
      return false;
    }
  }

  static String getUserName() {
    return "$firstName $lastName";
  }

  static AppUser getUser() {
    AppUser appUser = AppUser(
        id: userId,
        firstName: firstName,
        lastName: lastName,
        contactsIds: contactsIds,
        email: email,
        mobileNumber: mobileNumber,
        fieldList: fieldList);

    return appUser;
  }
}
