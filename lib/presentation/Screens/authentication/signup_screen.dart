import 'package:co_services_students/core/main_constant.dart';
import 'package:co_services_students/data/repositories/authentication_repository.dart';
import 'package:co_services_students/data/repositories/firestore_repository.dart';
import 'package:co_services_students/logic/cubit/navigation_cubit.dart';
import 'package:co_services_students/presentation/Screens/authentication/store.dart';
import 'package:co_services_students/presentation/Screens/authentication/verification_screen.dart';
import 'package:co_services_students/presentation/Screens/home_screen.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class SignUpScreen extends StatelessWidget {
  const SignUpScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    bool showPassword = false;
    return SafeArea(
      child: Scaffold(
        //TODO add back icon
        body: SingleChildScrollView(
          child: Container(
            decoration: const BoxDecoration(
              image: DecorationImage(
                image: AssetImage("assets/images/tob.jpg"),
                fit: BoxFit.cover,
              ),
            ),
            child: Padding(
              padding: EdgeInsets.only(left: 25.w, right: 25.w),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  SizedBox(
                    height: 80.h,
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        "Create your",
                        style: TextStyle(fontSize: 30.sp),
                      ),
                      Text(
                        "Account",
                        style: kh1TextStyle,
                      ),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        "First name",
                        style: kTextFieldLabelTextStyle,
                      ),
                      // SizedBox(height: 5.h),
                      TextFormField(
                        decoration: InputDecoration(
                          isDense: true,
                          fillColor: Colors.white,
                          hintText: "enter your first name",
                          hintStyle: TextStyle(color: Colors.grey, fontSize: 18.sp),
                          //errorText: " ",
                        ),
                        onChanged: (value) {
                          Store.firstName = value;
                        },
                      ),
                      const SizedBox(height: 15),
                      Text(
                        "Last name",
                        style: kTextFieldLabelTextStyle,
                      ),
                      // SizedBox(height: 5.h),
                      TextFormField(
                        decoration: InputDecoration(
                          isDense: true,
                          fillColor: Colors.white,
                          hintText: "enter your last name",
                          hintStyle: TextStyle(color: Colors.grey, fontSize: 18.sp),
                          //errorText: " ",
                        ),
                        onChanged: (value) {
                          Store.lastName = value;
                        },
                      ),
                      SizedBox(height: 15.h),
                      Text(
                        "Email",
                        style: kTextFieldLabelTextStyle,
                      ),
                      // SizedBox(height: 5.h),
                      TextFormField(
                        decoration: InputDecoration(
                          isDense: true,
                          fillColor: Colors.white,
                          hintText: "enter your email",
                          hintStyle: TextStyle(color: Colors.grey, fontSize: 18.sp),
                          //errorText: " ",
                        ),
                        onChanged: (value) {
                          Store.email = value;
                        },
                      ),
                      SizedBox(height: 15.h),
                      Text(
                        "Password",
                        style: kTextFieldLabelTextStyle,
                      ),
                      // SizedBox(height: 5.h),
                      TextFormField(
                        obscureText: showPassword,
                        decoration: InputDecoration(
                          suffixIcon: GestureDetector(
                            child: const Padding(
                              padding: EdgeInsets.only(right: 10),
                              child: Icon(
                                Icons.visibility_off, size: 22,
                                //: Icons.visibility,
                              ),
                            ),
                          ),
                          suffixIconColor: kBlackColor,
                          suffixIconConstraints: const BoxConstraints(
                            maxHeight: 20,
                          ),
                          isDense: true,
                          fillColor: Colors.white,
                          hintText: "enter your password",
                          hintStyle: TextStyle(color: Colors.grey, fontSize: 18.sp),
                          //errorText: " ",
                        ),
                        onChanged: (value) {
                          Store.password = value;
                        },
                      ),
                      SizedBox(height: 15.h),
                      Text(
                        "Phone number",
                        style: kTextFieldLabelTextStyle,
                      ),
                      // SizedBox(height: 5.h),
                      TextFormField(
                        decoration: InputDecoration(
                          isDense: true,
                          fillColor: Colors.white,
                          hintText: "enter your phone number",
                          hintStyle: TextStyle(color: Colors.grey, fontSize: 18.sp),
                          //errorText: " ",
                        ),
                        onChanged: (value) {
                          Store.mobileNumber = value;
                        },
                      ),
                      SizedBox(
                        height: 15.h,
                      )
                    ],
                  ),
                  Center(
                    child: Padding(
                      padding: EdgeInsets.only(bottom: 0.h),
                      child: ElevatedButton(
                        child: Text(
                          'CREATE ACCOUNT',
                          style: TextStyle(color: Colors.white, fontSize: 18.sp),
                        ),
                        onPressed: () async {
                          UserCredential result;
                          if (Store.validateInputs()) {
                            try {
                              result = await AuthenticationRepository.instance
                                  .signUp(Store.getUserName(), Store.email!, Store.password!, Store.mobileNumber!);
                              Store.userId = FirebaseAuth.instance.currentUser!.uid;
                              var user = Store.getUser();
                              print(user.toString());
                              await FirestoreRepository.instance.createUserDoc(user);
                              print(result);
                            } catch (e) {
                              print("error sign up:");
                              print(e);
                            }
                            await FirebaseAuth.instance.verifyPhoneNumber(
                              phoneNumber: '+963993333647',
                              verificationCompleted: (PhoneAuthCredential credential) {
                                FirebaseAuth.instance.currentUser!.updatePhoneNumber(credential);
                              },
                              verificationFailed: (FirebaseAuthException e) {
                                print('error ***');
                                print(e);
                                return;
                              },
                              codeSent: (String verificationId, int? resendToken) {
                                Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) {
                                  return VerificationScreen();
                                }));
                              },
                              codeAutoRetrievalTimeout: (String verificationId) {},
                            );
                            Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) {
                              return BlocProvider(
                                create: (context) => NavigationCubit(),
                                child: HomeScreen(),
                              );
                            }));
                          } else {
                            showDialog(
                              builder: (context) => AlertDialog(
                                content: Column(
                                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                  children: [
                                    Text("Error", style: TextStyle(fontSize: 25.sp, color: Colors.red)),
                                    const Center(
                                      child: Text("Wrong Inputs"),
                                    ),
                                  ],
                                ),
                              ),
                              context: context,
                            );
                          }
                        },
                        style: kButtonStyleSecondary,
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 10.h,
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
