import 'dart:ui';

import 'package:co_services_students/presentation/Screens/authentication/store.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import '../../../core/main_constant.dart';
import '../../widgets/verification_field.dart';

class VerificationScreen extends StatefulWidget {
  const VerificationScreen({Key? key}) : super(key: key);

  @override
  State<VerificationScreen> createState() => _VerificationScreenState();
}

class _VerificationScreenState extends State<VerificationScreen> {
  bool valid = false;
  TextEditingController verificationController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: Container(
          // constraints: BoxConstraints.expand(),
          decoration: const BoxDecoration(
            image: DecorationImage(
              image: AssetImage("assets/images/tob.jpg"),
              fit: BoxFit.cover,
            ),
          ),
          child: Padding(
            padding: EdgeInsets.only(left: 20.w, right: 20.w),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                GestureDetector(
                  onTap: () {},
                  child: SizedBox(
                    width: getScreenWidth(context),
                    height: 50.h,
                    child: Row(
                      children: [
                        const Icon(
                          Icons.arrow_back_ios_outlined,
                          color: Colors.white70,
                        ),
                        Text(
                          "Back",
                          style: TextStyle(color: Colors.white70, fontSize: 20.sp),
                        ),
                      ],
                    ),
                  ),
                ),
                SizedBox(
                  height: 60.h,
                ),
                Text(
                  "Phone Verification",
                  style: TextStyle(fontSize: 25.sp, fontWeight: FontWeight.bold, color: kGreenCOLOR),
                ),
                Padding(
                  padding: EdgeInsets.only(top: 15.h),
                  child: Text(
                    "We send verification code to",
                    style: TextStyle(fontSize: 16.sp, color: kGreenCOLOR),
                  ),
                ),
                Padding(
                  padding: EdgeInsets.only(bottom: 15.h),
                  child: Text(
                    "your number : ${Store.mobileNumber}",
                    style: TextStyle(fontSize: 16.sp, color: kGreenCOLOR),
                  ),
                ),
                ClipRRect(
                  child: SizedBox(
                    height: getScreenHeight(context) / 2,
                    width: getScreenWidth(context),
                    child: Stack(
                      alignment: Alignment.bottomCenter,
                      children: [
                        Column(
                          children: [
                            Padding(
                              padding: EdgeInsets.only(top: 20.h, bottom: 20.h),
                              child: VerificationField(
                                textEditingController: verificationController,
                                length: 6,
                                hasError: false,
                              ),
                            ),
                            const Spacer(),
                            Container(
                              height: 45.h,
                              width: getScreenWidth(context),
                              decoration: BoxDecoration(
                                border: Border.all(color: kGreenCOLOR),
                                borderRadius: BorderRadius.all(const Radius.circular(15.0)),
                                color: Colors.white,
                              ),
                              child: TextButton(
                                onPressed: () {
                                  //TODO rasoul
                                },
                                child: Text(
                                  "Resend code",
                                  style: TextStyle(color: kGreenCOLOR, fontSize: 18.sp),
                                ),
                              ),
                            ),
                          ],
                        ),
                        valid
                            ? BackdropFilter(
                                filter: ImageFilter.blur(
                                  sigmaX: 7.0,
                                  sigmaY: 7.0,
                                ),
                                child: Container(),
                              )
                            : Container()
                      ],
                    ),
                  ),
                ),
                Padding(
                  padding: EdgeInsets.only(top: 15.h, bottom: 15.h),
                  child: Container(
                    height: 45.h,
                    width: getScreenWidth(context),
                    decoration: const BoxDecoration(
                      borderRadius: BorderRadius.all(Radius.circular(15.0)),
                      color: kGreenCOLOR,
                    ),
                    child: TextButton(
                      onPressed: () {
                        //TODO rasoul
                        setState(() {
                          valid = !valid;
                          print(valid);
                        });
                      },
                      child: Text(
                        "Done",
                        style: TextStyle(color: Colors.white, fontSize: 18.sp),
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
