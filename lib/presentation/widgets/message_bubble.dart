import 'package:co_services_students/core/main_constant.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class MessageBubble extends StatelessWidget {
  final String text;
  final String? sender;
  final bool isMe;

  final String? imageUrl;

  MessageBubble({this.imageUrl, required this.sender, required this.text, required this.isMe});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(left: isMe ? 50.0 : 10, right: isMe ? 10 : 50, top: 10, bottom: 10),
      child: Column(
        crossAxisAlignment: isMe ? CrossAxisAlignment.end : CrossAxisAlignment.start,
        children: [
          /* sender != null
              ? Text(
                  ' from  $sender',
                  style: TextStyle(color: Colors.black54, fontSize: 12.sp),
                )
              : const SizedBox(),*/
          Material(
            borderRadius: isMe
                ? const BorderRadius.only(
                    topLeft: Radius.circular(10.0),
                    bottomLeft: Radius.circular(10.0),
                    bottomRight: Radius.circular(10.0),
                  )
                : const BorderRadius.only(
                    topRight: Radius.circular(10.0),
                    bottomLeft: Radius.circular(10.0),
                    bottomRight: Radius.circular(10.0),
                  ),
            elevation: 0,
            color: isMe ? Colors.yellow[300] : kGreenCOLOR,
            child: (imageUrl != null && imageUrl != "")
                ? SizedBox(
                    height: 200,
                    width: 250,
                    child: GestureDetector(
                      onTap: () {},
                      child: Padding(
                        padding: EdgeInsets.symmetric(vertical: 5.h, horizontal: 5.w),
                        child: ClipRRect(
                          borderRadius: isMe
                              ? const BorderRadius.only(
                                  topLeft: Radius.circular(10.0),
                                  bottomLeft: Radius.circular(10.0),
                                  bottomRight: Radius.circular(10.0),
                                )
                              : const BorderRadius.only(
                                  topRight: Radius.circular(10.0),
                                  bottomLeft: Radius.circular(10.0),
                                  bottomRight: Radius.circular(10.0),
                                ),
                          child: Image.network(
                            imageUrl!,
                            // 'assets/images/202-2nd-ave-hanover-pa-building-photo.jpg'
                          ),
                        ),
                      ),
                    ),
                  )
                : Padding(
                    padding: EdgeInsets.symmetric(vertical: 10.h, horizontal: 20.w),
                    child: Text(
                      text,
                      style: TextStyle(
                        color: isMe ? Colors.black54 : Colors.white,
                        fontSize: 15.sp,
                      ),
                    ),
                  ),
          ),
        ],
      ),
    );
  }
}
