import 'package:co_services_students/core/main_constant.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:pin_code_fields/pin_code_fields.dart';

class VerificationField extends StatefulWidget {
  VerificationField(
      {Key? key,
        required this.textEditingController,
        required this.hasError,
        required this.length})
      : super(key: key);

  TextEditingController textEditingController;
  bool hasError;
  int length;

  @override
  _VerificationFieldState createState() => _VerificationFieldState();
}

class _VerificationFieldState extends State<VerificationField> {
  String currentText = '';

  @override
  Widget build(BuildContext context) {
    return Center(
      child: SizedBox(
        width: getScreenWidth(context) - 100.w,
        child: PinCodeTextField(
          appContext: context,
          length: widget.length,
          obscureText: false,
          animationType: AnimationType.scale,
          pinTheme: PinTheme(
            shape: PinCodeFieldShape.underline,
            fieldWidth: 30.w,
            activeColor: Colors.green,
            inactiveColor: Colors.green,
            selectedColor: Colors.green,
            activeFillColor: Colors.white,
            selectedFillColor: Colors.white,
            inactiveFillColor: Colors.white,
          ),
          animationDuration: const Duration(milliseconds: 300),
          enableActiveFill: true,
          controller: widget.textEditingController,
          keyboardType: TextInputType.number,
          onCompleted: (v) {
            if (v.length != 4) {
              setState(() {
                widget.hasError = true;
              });
            } else {
              setState(() {
                widget.hasError = false;
              });
            }
            widget.textEditingController.text = v;
          },
          onChanged: (value) {
            setState(() {
              currentText = value;
            });
          },
        ),
      ),
    );
  }
}
