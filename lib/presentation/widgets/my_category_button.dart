import 'package:co_services_students/core/main_constant.dart';
import 'package:co_services_students/data/models/service.dart';
import 'package:co_services_students/data/repositories/firestore_repository.dart';
import 'package:co_services_students/logic/cubit/navigation_cubit.dart';
import 'package:co_services_students/presentation/widgets/service_card.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class MyCategoryButton extends StatelessWidget {
  final title;
  final icon;
  final onTap;

  final String category;

  const MyCategoryButton({Key? key, required this.category, this.title, this.icon, this.onTap}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        context.read<NavigationCubit>().pushToHome(ServicesByCategories(
              category: category,
            ));
      },
      child: Container(
        height: 50,
        color: Color(0xFFEEEEEE),
        child: Row(
          mainAxisSize: MainAxisSize.min,
          children: [
            Icon(
              icon,
              size: 35,
            ),
            SizedBox(
              width: 10,
            ),
            Flexible(
              child: Text(
                title,
                maxLines: 2,
                overflow: TextOverflow.ellipsis,
                style: TextStyle(fontSize: 18, color: Colors.grey),
              ),
            )
          ],
        ),
      ),
    );
  }
}

class ServicesByCategories extends StatelessWidget {
  final category;

  ServicesByCategories({Key? key, this.category}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: const BoxDecoration(
        image: DecorationImage(
          image: AssetImage("assets/images/tob.jpg"),
          fit: BoxFit.cover,
        ),
      ),
      child: Padding(
        padding: EdgeInsets.only(left: 20.w, right: 20.w),
        child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
          GestureDetector(
            onTap: () {
              print("back");
              context.read<NavigationCubit>().popFromHome();
            },
            child: SizedBox(
              width: getScreenWidth(context),
              height: 50.h,
              child: Row(
                children: [
                  const Icon(
                    Icons.arrow_back_ios_outlined,
                    color: Colors.white70,
                  ),
                  Text(
                    category,
                    style: TextStyle(color: Colors.white70, fontSize: 20.sp),
                  ),
                ],
              ),
            ),
          ),
          SizedBox(
            height: 20,
          ),
          Expanded(
              child: FutureBuilder(
            future: FirestoreRepository.instance.getServicesByCategory(category),
            builder: (context, snapshot) {
              if (!snapshot.hasData) {
                return Center(
                  child: CircularProgressIndicator(),
                );
              }
              List<Service> services = snapshot.data as List<Service>;

              return ListView.builder(
                itemCount: services.length,
                itemBuilder: (context, index) {
                  return Padding(
                    padding: const EdgeInsets.symmetric(vertical: 10.0),
                    child: ServiceCard(
                      service: services[index],
                      image: Image.network(
                        services[index].imageUrl ??
                            "https://www.pluralsight.com/content/pluralsight/en/blog/creative-professional/10-/10-essential-skills-need-motion-designer/_jcr_content/main/hero_blog_block/image-res.img.jpg/1527709354833.jpg",
                        fit: BoxFit.fitHeight,
                      ),
                      text: services[index].header,
                    ),
                  );
                },
              );
            },
          ))
        ]),
      ),
    );
  }
}
