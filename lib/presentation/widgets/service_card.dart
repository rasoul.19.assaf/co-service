import 'package:co_services_students/core/main_constant.dart';
import 'package:co_services_students/logic/cubit/navigation_cubit.dart';
import 'package:co_services_students/presentation/Screens/service_screen.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class ServiceCard extends StatelessWidget {
  final text;
  final image;

  final service;

  const ServiceCard({Key? key, this.text, this.image, this.service}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: GestureDetector(
        onTap: () {
          context.read<NavigationCubit>().pushToHome(ServiceScreen(
                service: service,
              ));
        },
        child: Container(
          height: 200,
          padding: EdgeInsets.all(18),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.stretch,
            // crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Align(
                alignment: Alignment.centerLeft,
                child: Text(
                  text ?? 'Motion graphic video design',
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    color: kBlackColor,
                  ),
                ),
              ),
              SizedBox(height: 10),
              Align(
                alignment: Alignment.centerRight,
                child: Container(
                  height: 130,
                  child: image,
                ),
              )
            ],
          ),
          decoration: BoxDecoration(
            color: Color(0xFFE1E1E1),
            borderRadius: BorderRadius.circular(50),
          ),
        ),
      ),
    );
  }
}
