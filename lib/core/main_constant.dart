import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

const Color kBlackColor = Color(0xff393939);

TextStyle kh1TextStyle = TextStyle(fontSize: 40.sp, fontWeight: FontWeight.bold);

ButtonStyle kButtonStyleSecondary = ElevatedButton.styleFrom(
  primary: kBlackColor,
  onPrimary: Colors.white,
  shape: StadiumBorder(),
  padding: EdgeInsets.symmetric(horizontal: 20, vertical: 20),
);

TextStyle kTextFieldLabelTextStyle = TextStyle(fontWeight: FontWeight.bold, fontSize: 18.sp);

const Color kGreyCOLOR = Color(0xffe1e1e1);
const Color kGreenCOLOR = Color(0xff1f8f00);
const Color kLightGreenCOLOR = Color(0xff72ae4a);


double getScreenWidth(BuildContext context) {
  return MediaQuery.of(context).size.width;
}

double getScreenHeight(BuildContext context) {
  return MediaQuery.of(context).size.height -
      MediaQuery.of(context).padding.top -
      MediaQuery.of(context).padding.bottom;
}
