import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:image_picker/image_picker.dart';

import 'main_constant.dart';

class ShowImageDialog extends StatelessWidget {
  const ShowImageDialog({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    ImagePicker picker = ImagePicker();
    return AlertDialog(
      title: Center(
        child: Text(
          'Add profile photo',
          style: TextStyle(fontSize: 25.sp, color: Colors.black),
        ),
      ),
      content: SizedBox(
        height: getScreenHeight(context) - 500.h,
        width: getScreenWidth(context),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            GestureDetector(
              onTap: () async {
                XFile? image =
                await picker.pickImage(source: ImageSource.gallery);
                Navigator.pop(context, image);
              },
              child: Row(
                children: <Widget>[
                  Icon(
                    Icons.drive_folder_upload,
                    color: Colors.black,
                    size: 30.sp,
                  ),
                  SizedBox(width: 10.w),
                  Text(
                    'Upload from gallery',
                    style: TextStyle(fontSize: 20.sp, color: Colors.black),
                  ),
                ],
              ),
            ),
            GestureDetector(
              onTap: () async {
                var im = await picker.pickImage(
                    source: ImageSource.camera,
                    preferredCameraDevice: CameraDevice.front);
                XFile? image = im;
                Navigator.pop(context, image);
              },
              child: Row(
                children: <Widget>[
                  Icon(
                    Icons.camera_alt_outlined,
                    color: Colors.black,
                    size: 30.sp,
                  ),
                  SizedBox(width: 10.w),
                  Text(
                    'Take a photo',
                    style: TextStyle(fontSize: 20.sp, color: Colors.black),
                  ),
                ],
              ),
            )
          ],
        ),
      ),
      backgroundColor: Colors.white,
      shape: const RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(15.0))),
    );
  }
}
