import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:co_services_students/data/models/app_user.dart';
import 'package:co_services_students/data/models/service.dart';
import 'package:co_services_students/data/providers/firestore_provider.dart';
import 'package:co_services_students/data/repositories/cloud_storage_repository.dart';
import 'package:firebase_auth/firebase_auth.dart' as auth;

class FirestoreRepository {
  static FirestoreRepository? _instance;
  final FirestoreProvider firestoreProvider = FirestoreProvider();
  //TODO:
  AppUser currentUser = AppUser();

  FirestoreRepository._privateConstructor();

  static FirestoreRepository get instance {
    _instance ??= FirestoreRepository._privateConstructor();

    return _instance!;
  }

  Future<void> createUserDoc(AppUser user) async {
    print("Creating user doc");
    currentUser = user;
    await firestoreProvider.createUserDocument(user.toJson());
  }

  Future<bool> initCurrentUser(auth.User user) async {
    DocumentSnapshot<Map<String, dynamic>> result = await firestoreProvider.getUserDocument(user.uid);
    print("getting user document");
    print(result.data());
    currentUser = AppUser.fromJson(result.data()!);
    currentUser.id = user.uid;
    return true;
  }

  Future<String> sendImage({required String filePath, required String fileName}) async {
    String cloudStoragePath = await CloudStorageRepository.instance.uploadFile(filePath: filePath, fileName: fileName);
    return cloudStoragePath;
  }

  void sendMessage(
      {required String message, required String receiverId, required String receiverName, String? imageUrl}) {
    /*  FirebaseFirestore.instance
        .collection("Users")
        .doc("Rub7qhAkUVGZWwD0WbqY")
        .collection("Messages")
        .where("other", isEqualTo: "FW7xJz8Wu9CgH4nCeULk")
        .get()
        .then((value) => print(value.docs[0].data()));*/

    if (currentUser.contactsIds == null || !currentUser.contactsIds!.contains(receiverId)) {
      initChat(receiverId);
    }
    FirebaseFirestore.instance.collection("Users").doc(currentUser.id).collection("Messages").add({
      "otherId": receiverId,
      "isSender": true,
      'createdAt': Timestamp.now(),
      "text": message,
      "imageUrl": imageUrl,
    });
    FirebaseFirestore.instance.collection("Users").doc(receiverId).collection("Messages").add({
      "otherId": currentUser.id,
      "isSender": false,
      'createdAt': Timestamp.now(),
      "text": message,
      "imageUrl": imageUrl,
    });
  }

  //TODO: take service model
  Future<bool> postService(Service service) async {
    return await firestoreProvider.postService(service);
  }

  Future<List<AppUser>> getContacts() async {
    print("getting contacts");
    List<AppUser> result = [];

    QuerySnapshot<Map<String, dynamic>> contacts;
    try {
      contacts = await FirebaseFirestore.instance
          .collection("Users")
          .where("contactsIds", arrayContains: currentUser.id)
          .get();
      for (var element in contacts.docs) {
        AppUser user = AppUser.fromJson(element.data());
        user.id = element.id;
        result.add(user);
      }
    } catch (e) {
      print(e);
    }
    print("result is");
    print(result);
    return result;
  }

  Future<List<Service>> getServices() async {
    var docs = await firestoreProvider.getAllServices();
    List<Service> services = [];
    for (var element in docs) {
      services.add(Service.fromJson(element.data()));
    }
    return services;
  }

  Future<List<Service>> getServicesByCategory(String category) async {
    var docs = await firestoreProvider.getServicesByCategory(category);
    List<Service> services = [];
    for (var element in docs) {
      services.add(Service.fromJson(element.data()));
    }
    return services;
  }

  Future<void> saveUserInfo() async {
    return await firestoreProvider.saveUserInfo(currentUser.toJson());
  }

  Future<void> initChat(otherId) async {
    await firestoreProvider.initChat(otherId);
    if (currentUser.contactsIds == null) {
      currentUser.contactsIds = [];
    }
    currentUser.contactsIds!.add(otherId);
  }
}

enum UserType {
  customer,
  serviceProvider,
}
