import 'dart:io';

import 'package:firebase_storage/firebase_storage.dart';

class CloudStorageRepository {
  FirebaseStorage storage = FirebaseStorage.instance;

  static CloudStorageRepository? _instance;

  CloudStorageRepository._privateConstructor();

  static CloudStorageRepository get instance {
    _instance ??= CloudStorageRepository._privateConstructor();
    return _instance!;
  }

  //Returns file url in the cloud storage
  Future<String> uploadFile({required String filePath, required String fileName}) async {
    File file = File(filePath);

    try {
      String path = 'services/$fileName';
      if (filePath.contains("/")) {
        path = fileName;
      }
      await storage.ref(path).putFile(file);
      print("URL ::: ");
      String url = await storage.ref(path).getDownloadURL();
      print(url);
      return url;
    } on FirebaseException catch (e) {
      print(e);
      return 'error';
    }
  }

  Future<ListResult> listFiles() async {
    ListResult results = await storage.ref('test').listAll();

    results.items.forEach((Reference ref) {
      print('found file: $ref');
    });
    return results;
  }

  Future<String> getFileUrl(String path) async {
    return await storage.ref(path).getDownloadURL();
  }

  Future<void> downloadImage(String path) async {
    await storage.ref(path).writeToFile(File(path)).whenComplete(() => true);
    try {
      String url = await storage.ref(path).getDownloadURL();
    } catch (e) {}
  }
}
