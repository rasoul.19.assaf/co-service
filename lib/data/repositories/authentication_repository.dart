import 'package:firebase_auth/firebase_auth.dart';

class AuthenticationRepository {
  FirebaseAuth auth = FirebaseAuth.instance;

  static AuthenticationRepository? _instance;

  AuthenticationRepository._privateConstructor();

  static AuthenticationRepository get instance {
    _instance ??= AuthenticationRepository._privateConstructor();
    return _instance!;
  }

  Future signIn(String email, String password) async {
    try {
      print("signing-in for $email");
      UserCredential userCredential = await auth.signInWithEmailAndPassword(
        email: email,
        password: password,
      );
      return userCredential.user;
    } on FirebaseAuthException catch (e) {
      if (e.code == 'weak-password') {
        print('The password provided is too weak.');
      } else if (e.code == 'email-already-in-use') {
        print('The account already exists for that email.');
      }
      print(e);
      return null;
    } catch (e) {
      print(e);
      return null;
    }
  }

  Future? signUp(String username, String email, String password, String phoneNumber) async {
    try {
      UserCredential userCredential = await auth.createUserWithEmailAndPassword(email: email, password: password);
      userCredential.user!.updateDisplayName(username);
      return userCredential;
    } on FirebaseAuthException catch (e) {
      if (e.code == 'weak-password') {
        print('The password provided is too weak.');
      } else if (e.code == 'email-already-in-use') {
        print('The account already exists for that email.');
      }
      print("error signup");
      print(e);
    } catch (e) {
      print("error signup");
      print(e);
    }
    return null;
  }

  bool checkSignedIn() {
    FirebaseAuth auth = FirebaseAuth.instance;

    if (auth.currentUser != null) {
      print("Welcome: ");
      print(auth.currentUser!.email);
      return true;
    }
    return false;
  }
}
