class Service {
  String? id;
  String? header;
  String? content;
  String? imageUrl;
  List<String>? categories;
  String? providerId;

  Service({this.id, this.header, this.content, this.imageUrl, this.categories, this.providerId});

  Service.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    header = json['header'];
    content = json['content'];
    imageUrl = json['imageUrl'];
    categories = [];
    if (json["categories"] != null) {
      for (String cat in json['categories']) {
        categories!.add(cat);
      }
    }
    providerId = json['providerId'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['header'] = this.header;
    data['content'] = this.content;
    data['imageUrl'] = this.imageUrl;
    data['categories'] = this.categories;
    data['providerId'] = this.providerId;
    return data;
  }
}
