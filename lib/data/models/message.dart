import 'package:cloud_firestore/cloud_firestore.dart';

class Message {
  String? otherId;
  bool? isSender;
  String? text;
  String? imageUrl;
  Timestamp? createdAt;

  Message({this.otherId, this.isSender, this.text, this.imageUrl, this.createdAt});

  Message.fromJson(Map<String, dynamic> json) {
    otherId = json['otherId'];
    isSender = json['isSender'];
    text = json['text'];
    imageUrl = json['imageUrl'];
    createdAt = json['createdAt'] as Timestamp;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['otherId'] = this.otherId;
    data['isSender'] = this.isSender;
    data['text'] = this.text;
    data['imageUrl'] = this.imageUrl;
    data['createdAt'] = this.createdAt;
    return data;
  }
}
