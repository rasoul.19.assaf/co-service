class AppUser {
  String? id;
  String? firstName;
  String? lastName;
  List<String>? contactsIds = [];
  String? email;
  String? mobileNumber;
  List<String>? fieldList = [];

  AppUser({this.id, this.firstName, this.lastName, this.contactsIds, this.email, this.mobileNumber, this.fieldList});

  AppUser.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    firstName = json['firstName'];
    lastName = json['lastName'];
    if (json['contactsIds'] != null) {
      contactsIds = [];
      for (var id in json['contactsIds'] as List<dynamic>) {
        contactsIds!.add(id as String);
      }
    }
    email = json['email'];
    mobileNumber = json['mobileNumber'];
    if (json['fieldList'] != null) {
      fieldList = [];
      for (var id in json['fieldList'] as List<dynamic>) {
        fieldList!.add(id as String);
      }
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['firstName'] = this.firstName;
    data['lastName'] = this.lastName;
    data['contactsIds'] = this.contactsIds ?? [];
    data['email'] = this.email;
    data['mobileNumber'] = this.mobileNumber;
    data['fieldList'] = this.fieldList ?? [];
    return data;
  }

  getFullName() {
    if (firstName == null || lastName == null) {
      return null;
    }
    return "$firstName $lastName";
  }
}
