import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:co_services_students/data/models/service.dart';
import 'package:co_services_students/data/repositories/firestore_repository.dart';

class FirestoreProvider {
  final FirebaseFirestore firestore = FirebaseFirestore.instance;

  Future<void> createUserDocument(Map<String, dynamic> json) async {
    await firestore.collection('Users').doc(json['id']).set(json);
  }

  Future<DocumentSnapshot<Map<String, dynamic>>> getUserDocument(String uid) async {
    return await firestore.collection("Users").doc(uid).get();
  }

  Future<List<QueryDocumentSnapshot<Map<String, dynamic>>>> getServicesByCategory(String category) async {
    print("getting + $category}");
    return await firestore
        .collection("Services")
        .where('categories', arrayContains: category)
        .get()
        .then((value) => value.docs);
  }

  void sendMessage(Map<String, dynamic> message, String id) {
    firestore.collection("Users").doc(id).collection('Messages').add(message);
  }

  Future<List<QueryDocumentSnapshot<Map<String, dynamic>>>> getOwnedProperties(String uid) async {
    return await firestore.collection('Services').where('providerId', isEqualTo: uid).get().then((value) => value.docs);
  }

  void addService(Map<String, dynamic> json) async {
    await firestore.collection('Services').add(json);
  }

  //TODO: call when first message
  Future<bool> initChat(String otherUserId) async {
    print("initializing chat");
    try {
      //TODO:
      await firestore.collection("Users").doc(FirestoreRepository.instance.currentUser.id).update({
        "contactsIds": FieldValue.arrayUnion([otherUserId]),
      });
      await firestore.collection("Users").doc(otherUserId).update({
        "contactsIds": FieldValue.arrayUnion([FirestoreRepository.instance.currentUser.id]),
      });
      return true;
    } catch (e) {
      print(e);
      return false;
    }
  }

  Future<bool> postService(Service service) async {
    print(service.toJson());
    var result = await firestore.collection("Services").add(service.toJson());
    print("result");
    return true;
    /*doc().set({
      "header": "header",
      "content": "content content content content content",
      "imageUrl":
      "https://d1rytvr7gmk1sx.cloudfront.net/wp-content/uploads/2018/10/istock-1135819437-740x450-1.jpg?x24468",
      "categories": [
        "programming and development",
        "edit videos and photos",
      ],
      "providerId": FirebaseAuth.instance.currentUser!.uid,
    });*/
  }

  Future<QuerySnapshot<Map<String, dynamic>>> getContacts(String id) async {
    print('test');
    return await firestore
        .collection("Users")
        .where("contactsIds", arrayContains: "w2QwQpn9xZctpcTR5Q3VWStbAb83")
        .get();
  }

  Future<List<QueryDocumentSnapshot<Map<String, dynamic>>>> getAllServices() async {
    return await firestore.collection("Services").get().then((value) => value.docs);
  }

  Future<void> saveUserInfo(Map<String, dynamic> json) async {
    await firestore.collection("Users").doc(json["id"]).set(json);
    return;
  }
}
