part of 'services_cubit.dart';

@immutable
abstract class ServicesState {
  List<Service> services = [];
}

class ServicesLoading extends ServicesState {}

class ServicesLoaded extends ServicesState {}

class ServicesInitial extends ServicesState {}
