import 'package:bloc/bloc.dart';
import 'package:flutter/cupertino.dart';
import 'package:meta/meta.dart';

part 'navigation_state.dart';

class NavigationCubit extends Cubit<NavigationState> {
  NavigationCubit() : super(NavigationInitial());

  void setIndex(int index) {
    print("setting index");
    state.index = index;
    final nState = NavigationInitial();
    nState.index = index;
    emit(nState);
  }

  void pushToHome(Widget widget) {
    state.homeNav.add(widget);
    final nState = NavigationInitial();
    nState.index = 0;
    nState.homeNav = state.homeNav;
    emit(nState);
  }

  void popFromHome() {
    print("poping");
    print(state.homeNav);
    if (state.homeNav.isNotEmpty) {
      state.homeNav.removeLast();
    }
    final nState = NavigationInitial();
    nState.index = 0;
    nState.homeNav = state.homeNav;
    emit(nState);
  }
}
