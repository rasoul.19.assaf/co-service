part of 'navigation_cubit.dart';

@immutable
abstract class NavigationState {
  int index = 0;
  List homeNav = [];
}

class NavigationInitial extends NavigationState {}
