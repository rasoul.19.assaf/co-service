import 'package:bloc/bloc.dart';
import 'package:co_services_students/data/models/service.dart';
import 'package:co_services_students/data/repositories/firestore_repository.dart';
import 'package:meta/meta.dart';

part 'services_state.dart';

class ServicesCubit extends Cubit<ServicesState> {
  ServicesCubit() : super(ServicesInitial()) {
    load();
  }

  void load() async {
    // print(FirebaseFirestore.instance
    //     .collection("Users")
    //     .where("contactsIds", arrayContains: "w2QwQpn9xZctpcTR5Q3VWStbAb83")
    //     .get()
    //     .then((value) => print(value.docs[0].data())));
    print(FirestoreRepository.instance.currentUser.id!);
    print("load");
    emit(ServicesInitial());
    ServicesLoaded newState = ServicesLoaded();
    newState.services = await FirestoreRepository.instance.getServices();
    print("loaded");

    emit(newState);
  }
}
